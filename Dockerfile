FROM nginx:1.23.3
COPY html/index.html /usr/share/nginx/html
COPY testec2pipeline.conf /usr/share/nginx/conf.d/testec2pipeline.conf
COPY nginx.conf /usr/share/nginx/nginx.conf