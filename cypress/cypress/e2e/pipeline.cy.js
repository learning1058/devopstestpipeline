describe('template spec', () => {
  it('Website is being served', () => {
    cy.readFile('../terraform/ec2_public_ip.txt').then(function(value) {
      const url = value.replace(/"|'/g, "")
      cy.visit(url)
      cy.contains('h1', 'Hello. I am served by NGINX')
    })
  })
})