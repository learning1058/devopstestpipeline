# S3 backend
terraform {
  backend "s3" {
    bucket = "miroslav-terraform-backend"
    key = "terraform/statefile"
    region = "eu-central-1"
  }
}

# VPC
resource "aws_vpc" "test_pipelice_vpc" {
  cidr_block = var.test_pipeline_vpc_cidr
  instance_tenancy = "default"
}

# IGW
resource "aws_internet_gateway" "test_pipelice_igw" {
  vpc_id = aws_vpc.test_pipelice_vpc.id
}

# Public subnet
resource "aws_subnet" "publicsubnet" {
  vpc_id = aws_vpc.test_pipelice_vpc.id
  cidr_block = "${var.public_subnet}"
}

# Private subnet
resource "aws_subnet" "privatesubnet" {
  vpc_id = aws_vpc.test_pipelice_vpc.id
  cidr_block = "${var.private_subnet}"
}

# Route table for public subnet
resource "aws_route_table" "publicrt" {
  vpc_id = aws_vpc.test_pipelice_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.test_pipelice_igw.id
  }
}

# Route table association for public RT
resource "aws_route_table_association" "publicrtAssociation" {
  subnet_id = aws_subnet.publicsubnet.id
  route_table_id = aws_route_table.publicrt.id
}

# Security group
resource "aws_security_group" "allow_all" {
  depends_on = [
    aws_vpc.test_pipelice_vpc
  ]
  name        = "allow_all"
  description = "Allow ALL inbound and outbound traffic"
  vpc_id = aws_vpc.test_pipelice_vpc.id

  ingress {
    description      = "ALL from VPC"
    from_port        = 0
    to_port          = 999
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow_all"
  }
}

resource "aws_instance" "web" {
  depends_on = [
    aws_security_group.allow_all
  ]
  ami = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"
  subnet_id = aws_subnet.publicsubnet.id
  associate_public_ip_address = true
  vpc_security_group_ids = ["${aws_security_group.allow_all.id}"]

  tags = {
    Name = "NGINX"
  }

 user_data = "${file("userdata.sh")}"

}


data "aws_ami" "ubuntu" {

    most_recent = true

    filter {
        name   = "name"
        values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"]
    }

    filter {
        name = "virtualization-type"
        values = ["hvm"]
    }

    owners = ["099720109477"]
}