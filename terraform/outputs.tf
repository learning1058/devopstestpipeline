output "ec2_ip" {
  value = "http://${aws_instance.web.public_ip}"
}
